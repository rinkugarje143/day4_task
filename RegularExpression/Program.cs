﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;

namespace RegualarExpression
{
    class Program
    {
        static void Main()
        {
            string mystr = "Today is Thursday\n 4th August\t Good Morning Team\n Let us learn Regular Expressions";
            Console.WriteLine( mystr);

            Match mymatch = Regex.Match(mystr, "day");
            Console.WriteLine(mymatch.Index);

            Console.WriteLine("**********");
            
           MatchCollection mymatches = Regex.Matches(mystr, "day",RegexOptions.IgnoreCase);
            Console.WriteLine($"Total count :{mymatches.Count}");


            Console.WriteLine("*********8*");
            MatchCollection mymatches1 = Regex.Matches(mystr,@"\d" ,RegexOptions.IgnoreCase);
            Console.WriteLine($"Total count :{mymatches1.Count}");
            
            foreach(Match item in mymatches1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("****************");
            mymatches1= Regex.Matches(mystr, @"\d{1,2}", RegexOptions.IgnoreCase);
            Console.WriteLine($"Total count :{mymatches1.Count}");

            foreach (Match item in mymatches1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("*************");
            mymatches1 = Regex.Matches(mystr, @"10[0,5]{1}", RegexOptions.IgnoreCase);
            Console.WriteLine($"Total count :{mymatches1.Count}");

            foreach (Match item in mymatches1)
            {
                Console.WriteLine(item);
            }


        }

    }
}