﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Constant
{
    internal enum InvoiceOption
    {
        Email,
        PDF,
        SMS,
        Print
    }
}
