﻿// See https://aka.ms/new-console-template for more information

using EnumDemo.Constant;
using EnumDemo.Model;
using EnumDemo.Repository;

Console.WriteLine("Here We are studying about Invoice Options in Enum");
UserRepository userRepositpory = new UserRepository();

//add products
Product product = new Product() { Name = "Dell", Price=50000, Quantity=2};
userRepositpory.Add(product);

//Generate Invoice
Console.WriteLine("------Invoice Format-------");
Console.WriteLine("1 is Email\n 2 is PDF\n 3 is SMS\n 4 is Print");
InvoiceOption invoiceOption=(InvoiceOption)Convert.ToInt32(Console.ReadLine());
userRepositpory.GenerateInvoice(invoiceOption);