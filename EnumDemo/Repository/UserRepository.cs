﻿using EnumDemo.Constant;
using EnumDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Repository
{
    internal class UserRepository:IOrderRepository, IBillable
    {
        Product[] products = new Product[20];
        int counter = 0;

        public void Add(Product product)
        {
            products[counter++] = product;
        }
        public void Delete(Product product)
        {
            throw new NotImplementedException();
        }
        public void GenerateInvoice(InvoiceOption invoiceOption)
        {
            int totalAmount = 0;
            foreach(Product items in products)
            {
                if (items == null) break;
                int amount = items.Price * items.Quantity;
                totalAmount += amount;
            }
            Console.WriteLine($"Your Total Bill is :{totalAmount}");
            switch (invoiceOption)
            {
                case InvoiceOption.Email:
                    Console.WriteLine("Invoice Email is done");
                    break;
                case InvoiceOption.PDF:
                    Console.WriteLine("Invoice pdf is generated");
                    break;
                case InvoiceOption.SMS:
                    Console.WriteLine("Invoice SMS sent to your Mobile Number");
                    break;
                case InvoiceOption.Print:
                    Console.WriteLine("Invoice Print is Ready");
                    break;

                default:
                    Console.WriteLine("Invalid......!!!!!!");
                    break;
            }
        }
        public Product[] GetProducts()
        {
            return products;
        }
        public void update(Product product)
        {
            throw new NotImplementedException();
        }

       
    }
}
