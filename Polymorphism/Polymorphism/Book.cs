﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    internal class Book
    {
        public void Viewdetails(string Name, string author)
        {
            Console.WriteLine($"Book Name is :{Name}\t Author :{author}");
        }

        public virtual string OrderStatus()
        {
            return "Download book ";
        }

        public void Viewdetails(string Name, string author, int Price)
        {
            Console.WriteLine($"Book Name is :{Name}\t Author :{author}\t Price is:{Price}");
        }
    }
}
