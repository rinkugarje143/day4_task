﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    internal class Ebook : Book
    {
        public override string OrderStatus()
        {
            return "Download book from Ebbok.com";
        }
   
    }
}
