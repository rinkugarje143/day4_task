﻿// See https://aka.ms/new-console-template for more information
//Mobile No should be 10 digit 
//Characters are not allowed
//should include countrey code, 91
//+ sign may be present or may not be
//The first digit of mobile number 6-9
//The rest of the digits shuld be in between 0-9

using System.Text.RegularExpressions;

public static class validate
{
    public const string pattern = @"^[+]?91[-\s]?[6-9][0-9]{9}$";

    public static bool validateMobileNum(string num)
    {
        if (num != null) return Regex.IsMatch(num, pattern);
        else return false;
    }
}

public class Program
{
    static void Main()
    {
        Console.WriteLine("Enter your Number :");
        string mobileNum=Console.ReadLine();
        Console.WriteLine(validate.validateMobileNum(mobileNum));
    }
}