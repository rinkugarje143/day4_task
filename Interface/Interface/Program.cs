﻿// See https://aka.ms/new-console-template for more information

using Interface.Repository;
using Interface.Model;

//Repository Instance
ProductRepository productRepository = new ProductRepository();

//Product Instance
Product product= new Product() { Name="laptop", Price=50000};
var cartStatus=productRepository.AddToCart(product);
Console.WriteLine(cartStatus);

//add 2nd element
product = new Product() { Name = "Mobile", Price = 40000 };
var cartstatus1 = productRepository.AddToCart(product);
Console.WriteLine(cartstatus1);

//add 3nd element
product = new Product() { Name = "TV", Price = 35000 };
var cartstatus2 = productRepository.AddToCart(product);
Console.WriteLine(cartstatus2);
Console.WriteLine("-------Show Alll items in array---------");
//call to booking Order method
List<Product> orderdetailes = productRepository.BookOrder();

foreach(var order in orderdetailes)
{
    Console.WriteLine( $"Name: {order.Name} Price :{order.Price}");
}
Console.WriteLine("---------Delete Items from array-----------");


//deleteing from array
Console.WriteLine("Give index for deleting ");
var index = int.Parse(Console.ReadLine());
List<Product>Deletlist=productRepository.DeleteProducts(index);
foreach(Product item in Deletlist)
{
    Console.WriteLine($"Name =Mobile\t Price = 40000");
}
Console.WriteLine("---------CAncel Order-----------");
//call cancel order
Console.WriteLine(productRepository.CancelOrder);
List<Product> CancelItem = productRepository.CancelOrder(index);
foreach (Product item in CancelItem)
{
    Console.WriteLine($"Name =Laptop\t Price = 50000");
}

//IOrder order = (IOrder)productRepository;
//order.BookOrder();
//order.CancelOrder();
