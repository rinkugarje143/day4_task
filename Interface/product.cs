﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    internal abstract class product
    {
        public string Name { get; set; }    
        public double Price { get; set; }

        //Create Constructor
        public product(string Name, double Price)
        {
            this.Name = Name;
            this.Price = Price;
        }
        //create method
    
        public abstract void AddToCard(int quantity);

    }
}
