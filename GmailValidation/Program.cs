﻿// See https://aka.ms/new-console-template for more information

using System.Text.RegularExpressions;

public static class validate
{
    public const string pattern = "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[@])(?=.*?[.a-z]).{9,}$";

    //public const string pattern = ("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
    //[#?!@$%^&*-] any special character
    //([a-z]) at least one lower case letter
    //([A-Z]) at least one upper case letter
    //{8-10}=length min 8 and max 10

    public static bool validatePassword(string mailID)
    {
        if (mailID != null) return Regex.IsMatch(mailID, pattern);
        else return false;
    }
}

public class Program
{
    static void Main()
    {
        Console.WriteLine("Enter your GmailID :");
        string passWord = Console.ReadLine();
        Console.WriteLine(validate.validatePassword(passWord));
    }
}
